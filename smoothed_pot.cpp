#include <Arduino.h>
#include "smoothed_pot.h"

SmoothedPot::SmoothedPot(uint8_t input_pin, float update_alpha, float update_delta)
    : input_pin(input_pin),
    smoothed_value(0.f), last_smoothed_value(0.f),
    update_alpha(update_alpha),
    update_delta(update_delta),
    was_reset(true),
    scale_factor(1.f / 1023.f)
{
    pinMode(input_pin, INPUT);
}

bool SmoothedPot::update()
{
    // Every input is mapped to the 0.0f - 0.1f and smoothed.
    uint16_t pot_value = analogRead(input_pin);
    float floatval = pot_value * scale_factor;
    float delta = (floatval - smoothed_value);

    if (was_reset || abs(delta) > 0.01f) {
        smoothed_value = last_smoothed_value = floatval;
        was_reset = false;
        return true;
    }

    smoothed_value += update_alpha * delta;
    smoothed_value = max(0.f, min(1.f, smoothed_value));

    if (abs(smoothed_value - last_smoothed_value) < update_delta)
        return false;

    last_smoothed_value = smoothed_value;
    return true;
}

void SmoothedPot::reset()
{
    was_reset = true;
}

float SmoothedPot::value() const {
    return last_smoothed_value;
}
