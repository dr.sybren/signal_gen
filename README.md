# Controller for Square Wave PWM Signal Generator

This project allows you to control the
[simple PWM signal generator](https://www.wanhunglo.com/2018/pwm-pulse-frequency-duty-cycle-adjustable-square-wave-signal-generator-module-whl-43)
that's sold
[pretty](https://www.ebay.com.sg/itm/Adjustable-PWM-Pulse-Frequency-Duty-Cycle-Square-Wave-Signal-Generator-Module-/263908235816)
[much](http://www.petpeoplesplace.com/petstore/5x-Signal-Generator-Pwm-Pulse-Frequency-Duty-Cycle-Adjustable-Lcd-1hz-150khz_401609981659.html)
[everywhere](https://www.scienceagogo.com/store/Pwm-Pulse-Frequency-Duty-Cycle-Adjustable-Module-Signal-Generator-Lcd-Display_223191783351.html)
with an Arduino.

The Arduino takes three analogue inputs from three potentiometers
(coarse frequency, fine frequency, duty cycle) and sends the desired parameters
to the Square Wave PWM Signal Generator via the TTL UART Serial connection.

## Findings

The serial connection to the PWM generator is rather sensitive to the communication speed. I've
tried to connect to it and type the commands by hand, but after the first letter it already sends a
`FAIL` response. Sending some commands with Python code worked.


## Info on the hardware

Here I've copy-pasted the same info you see on many sellers' websites, just for
reference.

### Description

PWM output, you can set the frequency, duty cycle;
Frequency is divided into four ranges, automatic switching:

- XXX (no decimal point): the smallest unit is 1Hz, the value range of 1Hz ~ 999Hz;
- X.XX (decimal point in the hundred) the smallest unit is 0.01Khz, the range of 1.00Khz ~ 9.99Khz;
- XX.X (decimal point in ten): the smallest unit is 0.1Khz; value range of 10.0KHz ~ 99.9KHz
- X.X.X (decimal point in ten and hundred): the smallest unit is 1Khz; value range 1KHz ~ 150KHz
- Duty cycle range: 0 ~ 100%;
- All set parameters, power-down automatically saved.

Examples:

- 100 indicates PWM output 100Hz pulse;
- 1.01 indicates PWM output 1.01K pulse;
- 54.1 indicates that the PWM output has a pulse of 54.1 kHz;
- 1.2.4 indicates that the PWM output is 124 kHz pulse;

### Parameters

- Working voltage: 3.3 ~ 30V;
- Frequency range: 1Hz ~ 150KHz;
- Frequency accuracy: the accuracy in each range is about 2%;
- Signal load capacity: the output current can be about 5 ~ 30ma;
- Output amplitude: PWM amplitude equal to the supply voltage;
- Ambient temperature: -20 ~ +70 ℃.

### Communication

- 9600 bps Data bits: 8
- Stop bit: 1
- Check digit: none
- Flow control: none

To set the frequency of the PWM, send:
- `F101`: Set the frequency to 101 HZ (001 to 999)
- `F1.05`: set the frequency of 1.05 KHZ (1.00 ~ 9.99)
- `F10.5`: Set the frequency to 10.5KHZ (10.0 ~ 99.9)
- `F1.0.5`: set the frequency of 105KHZ (1.0.0 ~ 1.5.0)

To set the PWM duty cycle, send:
- `DXXX`: set the PWM duty cycle to XXX; (001 ~ 100); Such as D050, set the PWM duty cycle is 50%

To read the current parameters, send `read`; the device will respond with two lines consisting of
the same as described above.

To acknowledge the received parameters, read:
- Set successfully return: `DOWN`;
- Setup failed to return: `FALL`.
