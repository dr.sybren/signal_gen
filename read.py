#!/usr/bin/env python3

import time

import serial

with serial.Serial('/dev/ttyUSB0') as ser:
    print(f'Communicating via {ser.name}')
    ser.write(b'read')

    def printline():
        print(ser.readline().decode().strip())

    printline()
    printline()

    ser.write(b'F001')
    printline()

    time.sleep(0.01)
    ser.write(b'D025')
    printline()
