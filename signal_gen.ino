#include <Arduino.h>
#include "smoothed_pot.h"

SmoothedPot pot_duty(A7, 0.5f, 0.01f);
SmoothedPot pot_freq(A6, 0.3f, 0.001f);
SmoothedPot pot_fine(A5, 0.2f, 0.001f);
char buffer[7];


void setup() {
    Serial.begin(9600);
    Serial.setTimeout(60);  // milliseconds
}

void loop_duty() {
    if (!pot_duty.update())
        return;

    uint8_t int_duty = 100 * pot_duty.value();
    snprintf(buffer, sizeof(buffer), "D%03d", int_duty);
    Serial.write(buffer);
    Serial.readString();
}

void loop_freq() {
    if (!pot_freq.update() && !pot_fine.update())
        return;

    float freq_value = pot_freq.value();
    float fine_value = pot_fine.value() - 0.5;
    if (freq_value < 0.25) {
        // 1 - 999 Hz
        int hz = freq_value * 4 * 998 + 1 + fine_value * 25;
        hz = max(1, hz);
        snprintf(buffer, sizeof(buffer), "F%03d", hz);
    }
    else if (freq_value < 0.50) {
        // 1000 - 9990 Hz
        int hz = (freq_value - 0.25) * 4 * 8990 + 1000 + 250 * fine_value;
        int thou = hz / 1000;
        int rest = hz % 1000 / 10;
        snprintf(buffer, sizeof(buffer), "F%01d.%02d", thou, rest);
    }
    else if (freq_value < 0.75) {
        // 10 - 99.9 KHz
        float khz = (freq_value - 0.50) * 4 * 89.9 + 10 + 2.5 * fine_value;
        int kilos = khz;
        int rest = (khz - kilos) * 10;
        snprintf(buffer, sizeof(buffer), "F%02d.%01d", kilos, rest);
    }
    else {
        // 100 - 150 KHz
        int khz = (freq_value - 0.75) * 4 * 50 + 100 + 25 * fine_value;
        khz = min(150, khz);
        int hundred = khz / 100;
        int ten = (khz % 100) / 10;
        int single = khz % 10;
        snprintf(buffer, sizeof(buffer), "F%d.%d.%d", hundred, ten, single);
    }

    Serial.write(buffer);
    Serial.readString();
}


void loop() {
    loop_duty();
    loop_freq();
}
