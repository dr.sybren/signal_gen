/* Read from a potentiometer and report a smoothed value.
 * 
 * The values are mapped from the Arduino's 0-1023 integer range to a
 * floating point 0.0-1.0 range (inclusive), and smoothened.
 *
 * update_alpha influences how instant changes are (1.0 = instant, no smoothing;
 * 0.1 = much smoothing, only uses 10% of the value).
 *
 * update_delta: when the last-reported value and the current value are less
 * than this apart, don't report an update at all.
 */
class SmoothedPot {
    uint8_t input_pin;
    float smoothed_value;
    float last_smoothed_value;
    float update_alpha;
    float update_delta;
    bool was_reset;
    const float scale_factor;
public:
    SmoothedPot(uint8_t input_pin, float update_alpha, float update_delta);

    /* Read from the input pin and maybe update the value reported by value().
     * When it returns false, the value will not have changed. When it returns
     * true, the value may have been. */
    bool update();

    /* This forces the next call to update() to not smooth and instantly go to
     * the value of the potentiometer. */
    void reset();

    /* Obtain the smoothed value of the potentiometer on a 0.f - 1.f scale. */
    float value() const;
};
